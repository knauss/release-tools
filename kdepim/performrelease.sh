#!/bin/sh

VERSION_STABLE="5.6.90"
VERSION_MASTER="5.7.40"
for i in ~/kdepim-dev/src/kde/pim/*/; do
	name=`basename $i`;
	if [ "$name" = "kdepim-runtime" ]; then
		continue
	fi
	if [ "$name" = "kleopatra" ]; then
		continue
	fi
	if [ "$name" = "syndication" ]; then
		continue
	fi
	echo "$name";
	cd $i
	git checkout master
	git reset --hard origin/master
	git pull
	git merge origin/Applications/17.12
	git checkout Applications/17.12
	git reset --hard origin/Applications/17.12
	git pull
	sed -e  "s/PIM_VERSION \".*\"/PIM_VERSION \"${VERSION_STABLE}\"/" -i CMakeLists.txt
	git add CMakeLists.txt
	git commit -m "GIT_SILENT: Prepare ${VERSION_STABLE}"
	git checkout master
	sed -e  "s/PIM_VERSION \".*\"/PIM_VERSION \"${VERSION_MASTER}\"/" -i CMakeLists.txt
	git add CMakeLists.txt
	git commit -m "GIT_SILENT: Open for future 5.8.x"
	git merge Applications/17.12 -s ours -m "Merge Applications/17.12"
	git push origin Applications/17.12 master
done;
