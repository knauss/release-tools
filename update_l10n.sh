#!/bin/bash

# Make a local checkout of l10n-something/* into a local dir

. utils.sh
. config
unset CDPATH

function update_or_create_l10n_checkout()
{
    local l10n_repo=$1
    local l10n_local_dir=$2

    if test -d $l10n_local_dir; then

        # Update existing checkout
        languages=`svn cat $l10n_repo/subdirs`
        for dir in $languages; do
            svn update $l10n_local_dir/$dir@
            if [ $? -ne 0 ]; then
                echo "failed updating!!!!"
                exit
            fi

            rev=`get_svn_rev $l10n_local_dir/$dir@`
            lang=`basename $dir`
            echo $lang > versions/kde-l10n-$lang
            echo $rev >> versions/kde-l10n-$lang
        done

    else

        # Make new checkout

        languages=`svn cat $l10n_repo/subdirs`
        for lang in $languages; do
            mkdir -p $l10n_local_dir/$lang
            # final @ required to handle languages like ca@valencia
            svn co $l10n_repo/$lang@ $l10n_local_dir/$lang

            rev=`get_svn_rev $l10n_local_dir/$lang@`
            echo $lang > versions/kde-l10n-$lang
            echo $rev >> versions/kde-l10n-$lang
        done

    fi
}

mkdir -p versions
update_or_create_l10n_checkout $l10n_repo5 $l10n_basedir5
